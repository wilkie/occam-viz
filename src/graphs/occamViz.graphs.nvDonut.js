/*global $, d3, Highcharts*/

function initOccamVizGraphsnvDonut(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Donut Chart",
    call:        "nvDonut",
    description: "An nv Donut chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvDonut = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;
	var n = 0;
	var color_index = 0;
	var mydata = options.data.groups.map(function (e, i){
		
		var dict = {};
		dict.label = e.name;
		dict.value = e.series;
		dict.color = options.colors[i];		
		return dict;
	
	
	});
	
nv.addGraph(function() {
  var chart = nv.models.pieChart()
      .x(function(d) { return d.label })
      .y(function(d) { return d.value })
      .showLabels(true)     //Display pie labels
      .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
      .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
      .color(options.colors)
	  .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
      ;

   d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})			
        .datum(mydata)
        .transition().duration(350)
        .call(chart);

  return chart;
});


  };
}
