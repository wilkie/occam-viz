/*global $, d3*/
function initOccamVizGraphsBars(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Bar Chart",
    call:        "bars",
    description: "A bar chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.bars = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    // Set up domains
    var minX = 0;
    var maxX = d3.max([options.labels.x.length - 1,
                       d3.max(options.data.groups, function(d) {
                          return d.series.length - 1;
                       })
                      ]);

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    var chart_attr = {
      margin: {
        top:    options.margin.top,
        left:   options.margin.left + 50,
        right:  options.margin.right,
        bottom: options.margin.bottom + (25 * (maxGroups+3) / 3)
      }
    };

    chart_attr.width  = options.width  - chart_attr.margin.left - chart_attr.margin.right;
    chart_attr.height = options.height - chart_attr.margin.top  - chart_attr.margin.bottom;

    var seriesSpace   = chart_attr.width / (maxX+1);
    var seriesPadding = seriesSpace * 0.1;
    var seriesWidth   = seriesSpace - (seriesPadding * 2);

    var barSpace      = seriesWidth / options.data.groups.length;
    var barPadding    = barSpace * 0.1;
    var barWidth      = barSpace - (barPadding * 2);

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
      .attr('width',  options.width  + 'px')
      .attr('height', options.height + 'px');

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + chart_attr.margin.left + ', ' + chart_attr.margin.top + ')');

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x)
                     .rangeBands([0, chart_attr.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, chart_attr.height]);

    // Bars
    var bars = chart.append('g')
                    .attr('class', 'bars');

    bars.selectAll('g')
      .data(d3.range(minX, maxX+1))      // For every x axis entry
      .enter().append('svg:g')     // Create a group
        .attr('class', 'group')
        .attr('transform', function(d, i) {
          return 'translate(' + (seriesSpace * i) + ', 0)';
        })
        .selectAll('rect')
        .data(options.data.groups) // For every group
        .enter().append('rect')    // Create a rectangle
          .attr('x', function(d, i) {
            return seriesPadding + barSpace * i + barPadding;
          })
          .attr('y', function(d, i, j) {
            // i is the index of the group we are in
            // j is the index of our x-axis position
            return y(d.series[j]) + 0.5;
          })
          .attr('data-group-index', function(d, i) { return i; })
          .attr('width', barWidth)
          .attr('height', function(d, i, j) {
            return chart_attr.height - y(d.series[j]);
          })
          .style({
            fill: function(d, i) {
              return options.colors[i];
            }
          });

  $(options.selector + " svg g.bars g.group rect")
    .on('mouseenter', function(e) {
      d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
        .style({
          "stroke":       "black",
          "stroke-width": "3px"
        });
    })

    .on('mouseout', function(e) {
      d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
        .style({
          "stroke":       "",
          "stroke-width": ""
        });
    });

    // Axis
    var xAxis = d3.svg.axis()
      .scale(x_series)
      .tickSize(6, 3, 1)
      .tickValues(options.labels.x);

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + chart_attr.height + ')')
      .call(xAxis);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = (chart_attr.width/3) * (i % 3) + seriesPadding + barPadding;
        var y = chart_attr.height + 10 + (25 * ((i+3)/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("rect")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("transform", "translate(0,-9)")
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });
  };
}
